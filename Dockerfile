FROM bash

# Direct download for version without CVEs
ARG JQ_VERSION
# Allow breaking cache, but do not set a default value as this always breaks the cache
ARG OCI_IMAGE_CACHE_TOKEN

# Add user 1000 as a member of root group
# libarchive-tools - bsdtar and others
RUN adduser -u 1000 -g "" -G root -D app &&\
  apk upgrade --update --no-cache &&\
  apk add --update --no-cache \
    curl \
    libarchive-tools \
    netcat-openbsd &&\
  curl -sfL -o /usr/local/bin/jq \
    https://github.com/jqlang/jq/releases/download/jq-$JQ_VERSION/jq-linux64 &&\
  chmod +x /usr/local/bin/*

# Add alias for scripts that are hard-coded to /bin/bash
RUN ln -s /usr/local/bin/bash /bin/bash

USER 1000
